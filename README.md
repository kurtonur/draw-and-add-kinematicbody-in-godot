# README #

  

This README would normally document whatever steps are necessary to get your application up and running.

  

### What is this repository for? ###



* Drawing a line is very easy when you use this script. Moreover, adding KinematicBody2D in tree when you finish to draw.

* Run demo scene in demo folder.

* Godot 3.2.1



### How do I get set up? ###



* Add Node2D and then add Draw.gd to that Node2D.

* Example Image

	![https://bitbucket.org/kurtonur/draw-and-add-kinematicbody-in-godot/raw/3ab3f6d43b4215e44d52b6cd932301628e3e0607/Images/img1.jpg](https://bitbucket.org/kurtonur/draw-and-add-kinematicbody-in-godot/raw/3ab3f6d43b4215e44d52b6cd932301628e3e0607/Images/img1.jpg)

* Change values that are shown in red box

* Inspector Image
 
	![https://bitbucket.org/kurtonur/draw-and-add-kinematicbody-in-godot/raw/3ab3f6d43b4215e44d52b6cd932301628e3e0607/Images/img2.jpg](https://bitbucket.org/kurtonur/draw-and-add-kinematicbody-in-godot/raw/3ab3f6d43b4215e44d52b6cd932301628e3e0607/Images/img2.jpg)

* Demo folder is already ready to test this project

#### Extra ####
Don`t forget to check Emulate Touch From Mouse in Project\General\Input Devices\Pointing if you want to test on editor or Windows