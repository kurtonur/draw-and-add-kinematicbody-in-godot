extends Node2D

const colors = [Color("#00d2ff"),Color("#928DAB"),
			Color("#2196f3"),Color("#f44336"),
			Color("#FF5F6D"),Color("#FFC371"),
			Color("#ff4b1f"),Color("#ff9068"),
			Color("#16BFFD"),Color("#CB3066"),
			Color("#EECDA3"),Color("#EF629F"),
			Color("#1D4350"),Color("#A43931"),
			Color("#a80077"),Color("#66ff00"),
			Color("#f7ff00"),Color("#db36a4"),
			Color("#ff4b1f"),Color("#1fddff"),
			Color("#BA5370"),Color("#F4E2D8"),
			Color("#4CA1AF"),Color("#C4E0E5"),
			Color("#4B79A1"),Color("#283E51"),
			Color("#0099F7"),Color("#F11712"),
			Color("#2980b9"),Color("#2c3e50"),
			Color("#5A3F37"),Color("#2C7744"),
			Color("#4DA0B0"),Color("#D39D38"),
			Color("#5614B0"),Color("#DBD65C"),
			Color("#2F7336"),Color("#AA3A38"),
			Color("#1e3c72"),Color("#2a5298"),
			Color("#114357"),Color("#F29492"),
			Color("#fd746c"),Color("#ff9068"),
			Color("#eacda3"),Color("#d6ae7b"),
			Color("#6a3093"),Color("#a044ff"),
			Color("#457fca"),Color("#5691c8"),
			Color("#B24592"),Color("#F15F79"),
			Color("#C02425"),Color("#F0CB35"),
			Color("#403A3E"),Color("#BE5869"),
			Color("#c2e59c"),Color("#64b3f4"),
			Color("#FFB75E"),Color("#ED8F03"),
			Color("#8E0E00"),Color("#1F1C18"),
			Color("#76b852"),Color("#8DC26F"),
			Color("#673AB7"),Color("#512DA8"),
			Color("#00C9FF"),Color("#92FE9D"),
			Color("#f46b45"),Color("#eea849"),
			Color("#e53935"),Color("#e35d5b"),
			Color("#005C97"),Color("#363795"),
			Color("#fc00ff"),Color("#00dbde"),
			Color("#CCCCB2"),Color("#757519"),
			Color("#2c3e50"),Color("#3498db"),
			Color("#304352"),Color("#d7d2cc")
			]

export(Color) var colorLine  = Color("#000000");
export(Color) var colorPoint  = Color("#000000");
export(bool) var colorRandom = false
export(int, 1, 40,1) var lineThickness = 5
export(int, 1, 40,1) var dotThickness = 5
export(int,1,100,1) var pointDistance = 50
export(int, 1, 250,1) var maxDot = 100
export(int, 1, 10,1) var maxLine = 10
export(bool) var showDot = false
export(bool) var gravityActive = false
export (NodePath) var addLineNode = "../"
export(String, FILE, "*.tscn") var createLineNode = "res://DrawLine/CreateLine.tscn"
var goodPoints =  PoolVector2Array();
var lastPoint = Vector2()
var dragIndex = 0 

func _ready():
	randomize()
	pass

func _draw():
	if(goodPoints.size()>= 2):
		for index_point in range(goodPoints.size()-1):
			draw_line(goodPoints[index_point], goodPoints[index_point + 1], colorLine,lineThickness,true)
			if showDot:
				draw_circle(goodPoints[index_point],dotThickness, colorPoint)
		draw_circle(goodPoints[0],dotThickness, colorPoint)
		draw_circle(goodPoints[goodPoints.size()-1],dotThickness, colorPoint)
	pass

func _input(event):
	ScreenTouch(event)
	pass
	
	
func ScreenTouch(event):
	if  (event is InputEventScreenTouch and event.is_pressed()) and dragIndex == 0:
		dragIndex = 1
		if colorRandom:
			colorLine = RandomColor()
			colorPoint = RandomColor()
	if (event is InputEventScreenTouch and !event.is_pressed()) and dragIndex == 1:
		dragIndex = 0
		
	if dragIndex == 1:
		if goodPoints.size() <= maxDot and maxLine > 0:
			EditPoints(get_canvas_transform().affine_inverse().xform(event.position))
	else:
		if goodPoints.size() >= 2 and maxLine > 0:
			AddLine2Tree(goodPoints)
			maxLine -= 1
		goodPoints = PoolVector2Array()
	update()
	pass
	
func AddLine2Tree(points):
	var newLine = load(createLineNode).instance()
	newLine.setPoints(points,lineThickness,colorLine)
	newLine.setGravity(gravityActive)
	get_node(addLineNode).add_child(newLine)
	pass

func EditPoints(point,speed = Vector2(0,0)):
	if(goodPoints.size() == 0):
		lastPoint = point
		goodPoints.push_back(point)
	else:
		var Range2zero = speed.distance_to(point)
		if(Range2zero <= 75):
			return
		if(getDistance(lastPoint,point)):
			lastPoint = point
			goodPoints.push_back(point)
	pass
	
func getDistance(vec1,vec2):
	var dist = vec1.distance_to(vec2)
	if dist < 0:
		dist *=-1
	if dist >pointDistance and dist < pointDistance*3:
		return true
	return false
	pass

func RandomColor():
	return colors[randi() % colors.size()]
	pass
