extends KinematicBody2D


const up= Vector2(0,-1)
const gravity = 5
var motion = Vector2()
var gravityActive = false

func _physics_process(delta):
	if(gravityActive):
		motion.y +=gravity
		motion=move_and_slide(motion,up)
	pass
	
func setPoints(points,thickness = 10,color = null):
	randomize()
	setCollision(points)
	if color:
		$line.default_color = color
	else:
		$line.default_color = Color(0,0,0,1)
	$line.width = thickness
	$line.points = points
	
func setCollision(points):
	for index_point in range(points.size()-1):
		var collision = CollisionShape2D.new()
		#collision.one_way_collision = true
		var segments = SegmentShape2D.new()
		var pointA: Vector2 = points[index_point]
		var pointB = points[index_point+1]
		segments.set_a(pointA)
		segments.set_b(pointB)
		collision.shape = segments
		self.add_child(collision)

func setGravity(value):
	gravityActive = value
	pass
